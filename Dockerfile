FROM quay.io/keboola/docker-custom-python:latest

COPY . /code/
# COPY /data/ /data/

RUN pip install --ignore-installed -r /code/requirements.txt

# CODE FOLDERS
WORKDIR /code/

# RUN THE MAIN PYTHON SCRIPT
CMD ["python", "-u", "/code/src/component.py"]
# iTunes Reviews Extractor

The iTunes Reviews extractor allows users to download reviews for any iTunes object (applications, songs, albumns, books, etc.) in Apple's database. The extractor downloads reviews for specified countries and incrementally saves them in Keboola Storage.

## API limitations

The extractor is limited to make 20 calls per minute, which is the direct limitation of the iTunes API. Moreover, due to storage reason, only 500 latest reviews are stored in iTunes API, hence only the latest 500 reviews can be downloaded for each object. 

**Note:** If the list of object and/or countries is too big, the extractor might be running for a couple of hours before finishing due to above API limitations.

## Configuration

Configuration of the extractor is very simple; all that's needed is a list of id's of objects and countries for which the reviews will be downloaded. The sample configuration can be found in the [extractor's repository](https://bitbucket.org/kds_consulting_team/kds-team.ex-itunes-reviews/src/master/component_config/sample-config/).

### Input table

The input table must be a table of **1 column** containing the id of the object. The extractor will loop over the list to download reviews for each specified object. If the 1 column specification is not met, the extractor will fail. The sample of the input table can be found [here](https://bitbucket.org/kds_consulting_team/kds-team.ex-itunes-reviews/src/master/component_config/sample-config/in/tables/test.csv).

### Countries

If the option `All countries` is selected, for each object reviews from all available countries is downloaded. Otherwise, only reviews for specified countries will be downloaded. 
If the extractor is configured via API, valid country codes must be provided. To download for all countries, use `ALL` option. See the [iTunes pages](https://affiliate.itunes.apple.com/resources/documentation/linking-to-the-itunes-music-store/#CountryCodes) for the list of all available countries and their respective country codes.

### Backfill

By default, the extractor only downloads reviews for objects, that were updated since the last successful run. By specifying `backfill` option to `true`, the extractor will download the latest 500 reviews (if available) regardless of the last successful run.

### Output

The output is a table of reviews for each object. The table is laoded incrementally into storage and uses `review_id`, `updated` and `country_code` columns as primary keys. Note that it is possible to have reviews with same `review_id` and `country_code`, but different `updated`, due to possibility of editing certain reviews. The expected output table can be found [here](https://bitbucket.org/kds_consulting_team/kds-team.ex-itunes-reviews/src/master/component_config/sample-config/out/tables/reviews.csv).

## Development

To run application locally or further develop it, run the following commands

```
docker-compose build
docker-compose run dev
```

which will build and run the `dev` image of application.
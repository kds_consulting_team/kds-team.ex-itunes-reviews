**0.1.0**
Added documentation and proper configuration description.
Cosmetic changes to code.
Ready for publication.

**0.0.3**
Added configuration schema

**0.0.2**
Added check for empty input and logging gelf.

**0.0.1**
Initial version of the component.
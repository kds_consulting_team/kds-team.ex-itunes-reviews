import csv
import logging
import os
import re
import sys
import lxml.etree
import logging_gelf.handlers
import logging_gelf.formatters
from lib.client import iTunesClient
from lib.app import iTunesObject
from kbc.env_handler import KBCEnvHandler

# Environment setup
sys.tracebacklimit = 0


# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")


logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])


APP_VERSION = '0.1.0'
KEY_COUNTRY_CODES = 'country_code'
KEY_BACKFILL_MODE = 'backfill_mode'
MANDATORY_PARS = [KEY_COUNTRY_CODES, KEY_BACKFILL_MODE]

ALL_COUNTRY_CODES = ['DZ', 'AO', 'AI', 'AG', 'AR', 'AM', 'AU', 'AT',
                     'AZ', 'BH', 'BD', 'BB', 'BY', 'BE', 'BZ', 'BM',
                     'BO', 'BW', 'BR', 'VG', 'BN', 'BG', 'CA', 'KY',
                     'CL', 'CN', 'CO', 'CR', 'CI', 'HR', 'CY', 'CZ',
                     'DK', 'DM', 'DO', 'EC', 'EG', 'SV', 'EE', 'FI',
                     'FR', 'DE', 'GH', 'GR', 'GD', 'GT', 'GY', 'HN',
                     'HK', 'HU', 'IS', 'IN', 'ID', 'IE', 'IL', 'IT',
                     'JM', 'JP', 'JO', 'KZ', 'KE', 'KR', 'KW', 'LV',
                     'LB', 'LI', 'LT', 'LU', 'MO', 'MK', 'MG', 'MY',
                     'MV', 'ML', 'MT', 'MU', 'MX', 'MD', 'MS', 'NP',
                     'NL', 'NZ', 'NI', 'NE', 'NG', 'NO', 'OM', 'PK',
                     'PA', 'PY', 'PE', 'PH', 'PL', 'PT', 'QA', 'RO',
                     'RU', 'SA', 'SN', 'RS', 'SG', 'SK', 'SI', 'ZA',
                     'ES', 'LK', 'KN', 'LC', 'VC', 'SR', 'SE', 'CH',
                     'TW', 'TZ', 'TH', 'BS', 'TT', 'TN', 'TR', 'TC',
                     'UG', 'GB', 'UA', 'AE', 'UY', 'US', 'UZ', 'VE',
                     'VN', 'YE']

OUT_FIELDS = ['object_id',
              'country_code',
              'review_id',
              'updated',
              'title',
              'content',
              'voteSum',
              'voteCount',
              'rating',
              'author_name',
              'author_uri']


class Component(KBCEnvHandler):

    def __init__(self, ALL_COUNTRY_CODES, OUT_FIELDS):

        KBCEnvHandler.__init__(self, MANDATORY_PARS)

        try:

            self.validate_config()

        except ValueError as e:

            logging.error(e)
            sys.exit(1)

        _cc = self.cfg_params[KEY_COUNTRY_CODES]

        if 'ALL' in _cc:

            self.country_codes = ALL_COUNTRY_CODES

        else:

            self.country_codes = _cc

        self.backfill = self.cfg_params[KEY_BACKFILL_MODE]
        self.client = iTunesClient()

        try:
            self._input_table_path = self.configuration.get_input_tables()[
                0]['full_path']
        except IndexError:

            logging.error("No input table provided.")
            sys.exit(1)

        self._output_table_path = os.path.join(self.tables_out_path, 'reviews.csv')
        self._writer = csv.DictWriter(open(self._output_table_path, 'w+'),
                                      fieldnames=OUT_FIELDS,
                                      delimiter=',',
                                      quotechar='"',
                                      quoting=csv.QUOTE_ALL,
                                      extrasaction='ignore',
                                      restval='')

        self._writer.writeheader()
        self._get_state_file()

    def _get_state_file(self):

        logging.debug("Backfill mode: %s" % str(self.backfill))

        if self.backfill is True:

            self.state = {}
            self.state_out = {}

        else:

            self.state = self.get_state_file()
            self.state_out = self.state

        logging.debug("State for the component run: %s" % str(self.state))

    def _get_first_page(self, itunes_obj):

        object_id = itunes_obj.id
        country = itunes_obj.country
        page_number = 1

        _xml_str = self.client._get_paged_results(
            country, object_id, page_number)

        return _xml_str

    def _build_xml_tree(self, xml_str):

        tree = lxml.etree.ElementTree(lxml.etree.fromstring(xml_str))

        return tree

    def _get_last_page(self, xml_tree):

        root = xml_tree.getroot()
        _last_tag = root.find("{http://www.w3.org/2005/Atom}*[@rel='last']")
        _last_link = _last_tag.attrib.get('href')

        if _last_link == '':

            return None

        else:

            _last_link = _last_link.split('?')[0]
            _rgxp_page = re.search(r'page=\d+', _last_link)

            if _rgxp_page is None:

                return None

            else:

                return _rgxp_page.group(0).split('=')[1]

    def _get_entries(self, xml_tree, state_updated):

        root = xml_tree.getroot()

        entries = root.findall('{http://www.w3.org/2005/Atom}entry')

        _out = []

        _state_reached = False
        for entry in entries:

            _updt = entry.find('{http://www.w3.org/2005/Atom}updated').text
            _id = entry.find('{http://www.w3.org/2005/Atom}id').text
            _title = entry.find('{http://www.w3.org/2005/Atom}title').text.replace('\n', '\\n')
            _content = entry.find('{http://www.w3.org/2005/Atom}content').text.replace('\n', '\\n')
            _voteSum = entry.find('{http://itunes.apple.com/rss}voteSum').text
            _voteCount = entry.find(
                '{http://itunes.apple.com/rss}voteCount').text
            _rtg = entry.find('{http://itunes.apple.com/rss}rating').text
            _author = entry.find('{http://www.w3.org/2005/Atom}author')

            _usr_name = _author.find('{http://www.w3.org/2005/Atom}name').text
            _usr_uri = _author.find('{http://www.w3.org/2005/Atom}uri').text

            if _updt > state_updated:

                _dict = {'review_id': _id,
                         'updated': _updt,
                         'title': _title,
                         'content': _content,
                         'voteSum': _voteSum,
                         'voteCount': _voteCount,
                         'rating': _rtg,
                         'author_name': _usr_name,
                         'author_uri': _usr_uri}

                _out += [_dict]

            else:

                _state_reached = True
                return _out, _state_reached

        return _out, _state_reached

    def run(self):

        with open(self._input_table_path) as _inpt:

            _rdr = csv.reader(_inpt)

            for _row in _rdr:

                if len(_row) != 1:

                    logging.error("Input table must be a single column table")
                    sys.exit(1)

                elif _rdr.line_num == 1:

                    continue

                _app_id = _row[0]
                _app_state = self.state.get(_app_id)
                _app_state = {} if _app_state is None else _app_state
                logging.info("State for application: %s" % str(_app_state))

                for _c_code in self.country_codes:

                    logging.info("Downloading reviews for object id %s from country %s." % (_app_id, _c_code))

                    _country_state = _app_state.get(_c_code)
                    logging.info("State for country: %s" % str(_country_state))

                    it_obj = iTunesObject(_app_id, _c_code, _country_state)
                    _first_page_xml = self._get_first_page(it_obj)
                    _first_page_tree = self._build_xml_tree(_first_page_xml)
                    _last_page = self._get_last_page(_first_page_tree)

                    if _last_page is None:

                        logging.info("No reviews for application %s in country %s" % (it_obj.id, it_obj.country))

                        continue

                    else:

                        it_obj._last_page = _last_page

                    if it_obj.state is None:

                        _state_updt = '1970-01-01T00:00:00-0700'

                    else:

                        _state_updt = it_obj.state

                    logging.debug("Original state: %s" % str(it_obj.state))
                    logging.debug("New state: %s" % str(_state_updt))

                    _app_dict = {'object_id': it_obj.id,
                                 'country_code': it_obj.country}

                    _state_reached = False
                    _max_page = int(it_obj._last_page) + 1
                    _max_updt = None
                    for p in range(1, _max_page):

                        _xml_str = self.client._get_paged_results(it_obj.country, it_obj.id, p)
                        _xml_tree = self._build_xml_tree(_xml_str)
                        _entries, _state_reached = self._get_entries(_xml_tree, _state_updt)

                        if p == 1 and len(_entries) != 0:

                            _max_updt = _entries[0]['updated']

                        for d in _entries:

                            _dict_to_write = {**d, **_app_dict}
                            self._writer.writerow(_dict_to_write)

                        if _state_reached is True:

                            break

                    self.state_out[it_obj.id] = {it_obj.country: _max_updt}

            self.write_state_file(self.state_out)
            self.configuration.write_table_manifest(self._output_table_path,
                                                    incremental=True,
                                                    primary_key=['review_id', 'updated', 'country_code'])


if __name__ == '__main__':

    logging.info("Running extractor version %s." % APP_VERSION)
    c = Component(ALL_COUNTRY_CODES, OUT_FIELDS)
    c.run()

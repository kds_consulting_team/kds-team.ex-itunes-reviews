import logging
import time
from kbc.client_base import HttpClientBase

BASE_URL = 'https://itunes.apple.com'


class iTunesClient(HttpClientBase):

    def __init__(self):

        HttpClientBase.__init__(self, BASE_URL, auth=None)

    def _get_paged_results(self, country_code, object_id, page_number):

        if page_number > 10:

            logging.warn("Max depth of RSS feed is enforced to 10.")

            return '<feed></feed>'

        url = self.base_url + f'/{country_code}/rss/customerreviews/page={page_number}' + \
            f'/id={object_id}/sortby=mostrecent/xml'

        rsp = self.get_raw(url)

        # Sleep for 3 seconds to ensure no more than 20 calls per minute are made.
        time.sleep(3)

        return rsp.content
